# Any Contributions are welcome!

This project is every-growing and can always use outside eyes and contributions! 
Just make sure you:
- document everything before you submit a merge request
    - Include your name, and the version number in the KDocs 
    (we follow Kotlin documentation guidelines with the addition of @/author and @/since)
        - If the version number at the time of your contribution is the 
        same as the enclosing class/interface/etc. it can be omitted.
