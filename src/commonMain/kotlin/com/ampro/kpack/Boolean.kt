/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package com.ampro.kpack

val Any.nil get() = null
val Any.unit get() = Unit

// Boolean extension functions

fun Boolean.toInt(): Int = if (this) 1 else 0

/**
 * Performs the given [action] when the [condition] is met.
 *
 * @param T
 * @param condition
 * @param action
 */
fun <T> T.`if`(condition: T.() -> Boolean, action: (T) -> Unit) =
    `if`(condition, action, {})

/**
 * Performs the given [action] when the [condition] is met, or the optional
 * [elseAction] when the [condition] is NOT met.
 *
 * @param T
 * @param condition
 * @param action
 * @param elseAction
 */
fun <T> T.`if`(
    condition: T.() -> Boolean,
    action: (T) -> Unit,
    elseAction: (T) -> Unit = {}
) = if (condition(this)) action(this) else elseAction(this)

/**
 * Performs the given [action] when the [condition] is NOT met.
 *
 * @param T
 * @param condition
 * @param action
 */
fun <T> T.ifNot(condition: T.() -> Boolean, action: (T) -> Unit) =
    ifNot(condition, action, {})

/**
 * Performs the given [action] when the [condition] is NOT met, or the optional
 * [elseAction] when the [condition] IS met.
 *
 * @param T
 * @param condition
 * @param action
 * @param elseAction
 */
fun <T> T.ifNot(
    condition: T.() -> Boolean,
    action: (T) -> Unit,
    elseAction: (T) -> Unit = {}
) = `if`({ !condition(this) }, action, elseAction)
