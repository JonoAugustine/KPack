package com.ampro.kpack

/********************
 *   Regex & String
 * ******************/

/**
 * Add a [regex] pattern to the end of this [Regex].
 *
 * @param regex The regex to concat
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
operator fun Regex.plus(regex: Regex) = Regex(pattern + regex.pattern)

/**
 * Add a regex [pattern] to the end of this [Regex].
 *
 * @param pattern The regex to concat
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
operator fun Regex.plus(pattern: String) = Regex(this.pattern + pattern)

/**
 * @param set a [Regex] paired to the [String] with which to replace it with
 */
fun String.replace(vararg set: Pair<Regex, String>): String {
    var s = this
    set.forEach { s = s.replace(it.first, it.second) }
    return s
}

/**
 * @param set a [Regex] paired to the [String] with which to replace it with
 */
fun String.replaceSet(vararg set: Pair<String, String>): String {
    var s = this
    set.forEach {
        s = s.replace(it.first.toRegex(), it.second)
    }
    return s
}

/**
 * Returns this [String] with all instances of the given [regex] removed.
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
fun String.removeAll(regex: Regex) = this.replace(regex, "")

/**
 * Returns this [String] with all instances of the given regex [pattern] removed.
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
fun String.removeAll(pattern: String) = this.replace(pattern.toRegex(), "")

infix fun String.matchesAny(regecies: Collection<Regex>): Boolean {
    regecies.forEach { if (this.matches(it)) return true }
    return false
}

infix fun String.matches(regex: String) = this.matches(regex.toRegex())

fun String.matchesAny(vararg regecies: Regex): Boolean {
    regecies.forEach { if (this.matches(it)) return true }
    return false
}

fun String.matchesAnyConfirm(regecies: Collection<Regex>): List<Regex> {
    val list = mutableListOf<Regex>()
    regecies.forEach { if (this.matches(it)) list.add(it) }
    return list
}

fun List<String>.contains(regex: Regex): Boolean {
    forEach { if (it.matches(regex)) return true }
    return false
}

fun String.containsAny(strings: Collection<String>, ignoreCase: Boolean = true):
        Boolean = strings.asSequence().any { this.contains(it, ignoreCase) }

operator fun String.times(repetitions: Int) = this.repeat(repetitions)
