/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package com.ampro.kpack.collections

import com.ampro.kpack.unit

/**
 * A data entry node for a [LinkedList].
 *
 * @param T Data type
 * @property data The [data][T]
 * @property next The next [Node]
 * @property prev The previous [Node]
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
open class Node<T : Any> internal constructor(
    var data: T? = null,
    var next: Node<T>? = null,
    var prev: Node<T>? = null
)

fun <T> linkedListOf(vararg elements: T) = LinkedList(elements)

/**
 * A Linked list is a linear collection of data elements, whose order is not given
 * by their physical placement in memory. Instead, each element points to the next.
 * It is a data structure consisting of a collection of [nodes][Node] which
 * together represent a sequence. In its most basic form, each node contains:
 * [data][T], and a reference (in other words, a link) to the next [Node] in the
 * sequence. This structure allows for efficient insertion or removal of elements
 * from any position in the sequence during iteration.
 *
 * TODO functions to move existing elements by index or by front/back
 *
 * @param T data type
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
open class LinkedList<T : Any>() : Collection<T> {

    /**
     * Construct a [LinkedList] of the given [size] and elements created with
     * the given [function][init].
     */
    constructor(size: Int, init: (Int) -> T) : this() {
        (0 until size).forEach { append(init(it)) }
    }

    constructor(source: Iterable<T>) : this() {
        this.appendAll(source)
    }

    /** Construct a [LinkedList] with the given parameter [elements]. */
    constructor(vararg elements: T) : this() {
        elements.forEach { append(it) }
    }

    protected open val head: Node<T> = Node()
    protected open val tail: Node<T> = Node(prev = head).also { head.next = it }

    /** The fist element in the list or `null` if the list is empty. */
    open val front: T? get() = head.next?.data
    /** The back element in the list or `null` if the list is empty. */
    open val back: T? get() = tail.prev?.data

    /** The current number of elements in the list. */
    override var size: Int = 0

    /** Add the [data] after the given [node]. */
    protected open fun addAfter(node: Node<T>, data: T): Node<T> =
        Node(data, node.next, node).also {
            node.next?.prev = it
            node.next = it
            size++
        }

    /**
     * Add the [element] to the front of the [LinkedList].
     *
     * @param element The data to add.
     * @return `true`
     */
    open fun prepend(element: T) = addAfter(head, element).unit

    /**
     * Add the [element] to the end of the [LinkedList].
     *
     * @param element The data to add.
     * @return `true`
     */
    open fun append(element: T) = addAfter(tail.prev!!, element).unit

    /**
     * Prepend the [elements] to the end of the list.
     *
     * @param elements The data to add.
     * @return `true` if the [elements] were added.
     */
    open fun prependAll(elements: Iterable<T>) = elements.forEach { prepend(it) }

    /**
     * Append the [elements] to the end of the list.
     *
     * @param elements The data to add.
     * @return `true` if the [elements] were added.
     */
    open fun appendAll(elements: Iterable<T>) = elements.forEach { append(it) }

    /**
     * Add the [element] to the end of the [LinkedList].
     *
     * @param element The data to add.
     * @return `true`
     */
    open operator fun plusAssign(element: T) {
        append(element)
    }

    /** Get the [Node] at the [index]. */
    protected open fun getNode(index: Int): Node<T> {
        if (size == 0 || index !in 0 until size) {
            throw IndexOutOfBoundsException("index: $index, size: $size")
        }
        return when (index) {
            size - 1 -> return tail.prev!!
            0 -> head.next!!
            else -> nodeIterator().asSequence().elementAt(index)
        }
    }

    /**
     * Returns the [value][T] at the specified [index].
     *
     * @param index The index to query.
     * @throws IndexOutOfBoundsException if the [index] is not in the size of the list.
     */
    open operator fun get(index: Int): T = getNode(index).data!!

    open operator fun minusAssign(element: T) {
        remove(element)
    }

    open fun removeAt(index: Int): T {
        val node = getNode(index)
        node.prev!!.next = node.next
        node.next!!.prev = node.prev
        size--
        return node.data!!
    }

    open fun remove(element: T): T? {
        nodeIterator().forEach { n ->
            if (n.data == element) {
                n.prev!!.next = n.next
                n.next!!.prev = n.prev
                size--
                return n.data
            }
        }
        return null
    }

    open fun removeFirst(): T? {
        if (size == 0) return null
        // [h] <-> [n] <-> [nn] ...
        // [h] <-> [nn] ...
        val k = head.next!!.data!!
        head.next!!.next!!.prev = head
        head.next = head.next!!.next
        size--
        return k
    }

    open fun removeLast(): T? {
        if (size == 0) return null
        // ... <-> [pp] <-> [p] <-> [t]
        // ... <-> [pp] <-> [t]
        val k = tail.prev!!.data!!
        tail.prev!!.prev!!.next = tail
        tail.prev = tail.prev!!.prev
        size--
        return k
    }

    /** Remove all elements from the list which are in the given [elements]. */
    open fun removeAll(elements: Iterable<T>) {
        elements.forEach { remove(it) }
    }

    /** Remove all elements from the list which are not in the given [elements]. */
    open fun retainAll(elements: Iterable<T>) =
        removeAll(filterNot { elements.contains(it) })

    /** Remove all elements from the [LinkedList]. */
    open fun clear() {
        head.next = tail
        tail.prev = head
        size = 0
    }

    override fun isEmpty() = size == 0

    override fun contains(element: T) = any { it == element }

    override fun containsAll(elements: Collection<T>) = elements.all(this::contains)

    private fun nodeIterator(): Iterator<Node<T>> = object : Iterator<Node<T>> {
        private var node: Node<T> = head.next!!

        override fun hasNext(): Boolean = node != tail

        override fun next(): Node<T> {
            if (!hasNext()) throw NoSuchElementException()
            val out = node
            node = node.next!!
            return out
        }
    }

    override fun iterator(): Iterator<T> = object : Iterator<T> {
        private val it = nodeIterator()
        override fun hasNext(): Boolean = it.hasNext()
        override fun next(): T = it.next().data!!
    }

    /**
     * Returns an [Iterator] starting from the back element
     * and ending on the front.
     */
    fun inverseIterator() = object : Iterator<T> {

        private var current: Node<T> = tail.prev!!

        override fun hasNext() = current != head

        override fun next(): T {
            if (!hasNext()) throw NoSuchElementException()
            val out = current.data!!
            current = current.prev!!
            return out
        }
    }
}
