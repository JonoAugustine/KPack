/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package com.ampro.kpack.collections

/**
 * An [Edge] represents the vector between two vertices of type [V].
 *
 * @param T Edge type
 * @param V Vertex type
 * @property data Data contained in the [Edge]
 * @property from Origin vertex
 * @property to destination vertex
 */
open class Edge<T, V>(
    var data: T,
    var from: V,
    var to: V
) {
    override fun equals(other: Any?): Boolean =
        other != null && other is Edge<*, *> &&
                other.data == data &&
                other.from == from && other.to == to

    override fun hashCode(): Int = data.hashCode() + from.hashCode() + to.hashCode()
}

/**
 * TODO
 *
 * @param V Vertex data type
 * @param E Edge data type
 */
open class Graph<V, E>() { // : Iterable<V> {

    /**
     *
     * @property outGoing Outgoing [Edge]s.
     * @property inComing Incoming [Edge]s.
     * @property size The total number of edges both [inComing] and [outGoing].
     * @property isEmpty `true` if there are no [outGoing] or [inComing] edges.
     *
     * @author Jonathan Augustine
     * @since 1.0.0
     */
    open inner class EdgeSet(
        val outGoing: HashSet<Edge<E, V>> = hashSetOf(),
        val inComing: HashSet<Edge<E, V>> = hashSetOf()
    ) {
        val size: Int get() = outGoing.size + inComing.size
        val isEmpty: Boolean = size == 0
    }

    /** Map of vertices to [EdgeSet]s. */
    protected val adjacency = HashMap<V, EdgeSet>()

    val vertexCount: Int get() = adjacency.size
    var edgeCount: Int = 0
    val isEmpty: Boolean get() = vertexCount == 0
    val hasNoConnections: Boolean get() = edgeCount == 0

    /** Construct a new [Graph] and all all values from the [source] graph. */
    constructor(source: Graph<V, E>) : this() {
        adjacency.putAll(source.adjacency)
    }

    open fun putVertex(data: V): EdgeSet? = adjacency.put(data, EdgeSet())

    open fun getOrPutVertex(data: V): EdgeSet = adjacency.getOrPut(data, { EdgeSet() })

    open fun remove(vertex: V): EdgeSet? {
        val set = findOrNull(vertex) ?: return null
        set.inComing.forEach { this[it.from].outGoing.remove(it) }
        set.outGoing.forEach { this[it.to].inComing.remove(it) }
        return adjacency.remove(vertex)
    }

    open fun connect(edgeData: E, from: V, to: V) {
        val (fromSet, toSet) = find(from).outGoing to find(to).inComing
        val e = Edge(edgeData, from, to)
        if (fromSet.any { it.to == to } || (e in fromSet || e in toSet)) {
            throw IllegalStateException("Duplicate edges not allowed.")
        }
        fromSet.add(e)
        toSet.add(e)
        edgeCount++
    }

    open infix fun Pair<V, V>.by(edge: E) = connect(edge, this.first, this.second)

    open fun getConnection(origin: V, destination: V): E? {
        val from = findOrNull(origin)?.outGoing ?: return null
        val to = findOrNull(destination)?.inComing ?: return null
        return from.firstOrNull { it in to }?.data
    }

    open infix fun V.getEdgeTo(destination: V): E? =
        getConnection(this, destination)

    open fun disconnect(origin: V, destination: V): Boolean {
        val fromSet = findOrNull(origin)?.outGoing ?: return false
        val toSet = findOrNull(destination)?.inComing ?: return false
        return (fromSet.removeAll { it.to == destination } ||
                toSet.removeAll { it.from == origin }).also {
            if (it) edgeCount--
        }
    }

    open fun disconnect(edge: E, origin: V, destination: V): Boolean {
        val fromSet = findOrNull(origin)?.outGoing ?: return false
        val toSet = findOrNull(destination)?.inComing ?: return false
        val e = fromSet.firstOrNull { it.data == edge } ?: return false
        return (fromSet.remove(e) || toSet.remove(e)).also {
            if (it) edgeCount--
        }
    }

    open infix fun V.connectedTo(destination: V): Boolean =
        getConnection(this, destination) != null

    open fun findOrNull(vertex: V): EdgeSet? = adjacency[vertex]

    /*override fun iterator(): Iterator<V> = object : Iterator<V> {



        override fun hasNext(): Boolean {
            TODO("not implemented")
        }

        override fun next(): V {
            TODO("not implemented")
        }
    }*/
}

fun <V, E> Graph<V, E>.find(vertex: V): Graph<V, E>.EdgeSet = findOrNull(vertex)
    ?: throw NoSuchElementException("No Vertex found at $vertex.")

operator fun <V, E> Graph<V, E>.get(vertex: V): Graph<V, E>.EdgeSet = find(vertex)

operator fun <V, E> Graph<V, E>.set(origin: V, destination: V, edge: E) =
    connect(edge, origin, destination)

operator fun <V, E> Graph<V, E>.contains(vertex: V): Boolean = findOrNull(vertex) != null

// TODO path finder
