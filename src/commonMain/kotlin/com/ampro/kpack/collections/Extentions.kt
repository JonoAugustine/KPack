/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package com.ampro.kpack.collections

import kotlin.random.Random

/* ***********************
      List Extensions
 *************************/

/**
 * Remove and return the back entry of the [list][MutableList]. `null` if empty.
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
fun <E> MutableList<E>.removeLastOrNull() = if (isEmpty()) null else removeAt(size - 1)

/**
 * Splits an Iterable into two lists, one matching the [predicate] and the other not
 * matching.
 *
 * @return [Pair] of mutable lists. First element contains those matching the predicate.
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
infix fun <E> Iterable<E>.splitBy(predicate: (E) -> Boolean):
        Pair<MutableList<E>, MutableList<E>> {
    return (filter(predicate).toMutableList() to filterNot(predicate).toMutableList())
}

fun <E> MutableList<E>.removeRandom(seed: Int = 0): E =
    removeAt(Random(seed).nextInt(size))


fun <T> List<T>.subList(fromIndex: Int): MutableList<T> {
    return if (this.isNotEmpty()) subList(fromIndex, size).toMutableList()
    else mutableListOf()
}

/* ***********************
        Map Extensions
 *************************/

fun <K, V> MutableMap<K, V>.removeIf(predicate: (K, V) -> Boolean) {
    val targets = filter { predicate(it.key, it.value) }
    targets.forEach { t -> this.remove(t.key) }
}
