/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package com.ampro.kpack.test

import com.ampro.kpack.times
import com.soywiz.klock.TimeSpan
import com.soywiz.klock.measureTime
import com.soywiz.klock.measureTimeWithResult
import kotlinx.coroutines.withTimeoutOrNull
import kotlin.reflect.KClass

@DslMarker
annotation class BenchmarkDsl

@Target(AnnotationTarget.FUNCTION)
annotation class BeforeBench

@Target(AnnotationTarget.FUNCTION)
annotation class AfterBench

class KBench<T>(
    val name: String,
    val iterations: Int = DEFAULT_ITERATIONS,
    val timeOutMilli: Long = DEFAULT_TIMEOUT_MS,
    val expected: List<KClass<out Exception>> = mutableListOf(),
    val pre: ((T) -> Unit)? = null,
    val post: ((T) -> Unit)? = null,
    val bench: (T) -> Unit
) {
    companion object {
        var DEFAULT_ITERATIONS: Int = 10_000
        var DEFAULT_TIMEOUT_MS: Long = 20_000
    }
}

class FunctionNotFoundException(name: String? = null) : Exception(name)

expect fun runTest(f: suspend () -> Unit): Unit

/**
 * TODO Automatic incremental iteration increase
 *
 * @param T
 * @constructor
 * TODO
 *
 * @param init
 */
abstract class Benchmarker<T : Any>
@BenchmarkDsl constructor(init: Benchmarker<T>.() -> Unit) {

    var verbose_log: Boolean = true
    private fun log(any: Any) = println(any)
    private fun verbose(any: Any) = if (verbose_log) println(any) else Unit

    var default_iterations: Int = KBench.DEFAULT_ITERATIONS
    var default_timout: Long = KBench.DEFAULT_TIMEOUT_MS

    val _benchmarks = mutableListOf<KBench<T>>()
    val _after = mutableListOf<Pair<String, (T) -> Unit>>()
    val _before = mutableListOf<Pair<String, (T) -> Unit>>()

    init {
        this.apply(init)
    }

    @BenchmarkDsl
    constructor(vararg benchmarks: KBench<T>) : this({}) {
        _benchmarks.addAll(benchmarks)
    }

    /** Function to generate a new test object. */
    lateinit var generate: () -> T

    /** Add a [KBench] with the name of the invoking string. */
    @BenchmarkDsl
    operator fun String.invoke(bench: (T) -> Unit): KBench<T> =
        this@invoke(
            iterations = default_iterations,
            timeOutMilli = default_timout,
            bench = bench
        )

    /** Add a [KBench] with the name of the invoking string. */
    @BenchmarkDsl
    operator fun String.invoke(
        iterations: Int = default_iterations,
        timeOutMilli: Long = default_timout,
        vararg expected: KClass<out Exception>,
        bench: (T) -> Unit
    ): KBench<T> = KBench(
        this, iterations, timeOutMilli,
        expected = expected.asList(), bench = bench
    ).also { _benchmarks.add(it) }

    @BenchmarkDsl
    operator fun String.invoke(
        iterations: Int = 10_000,
        timeOutMilli: Long = 20_000,
        vararg expected: KClass<out Exception>,
        pre: ((T) -> Unit)? = null,
        post: ((T) -> Unit)? = null,
        bench: (T) -> Unit
    ): KBench<T> = KBench(
        this, iterations, timeOutMilli, expected.asList(), pre, post, bench
    ).also { _benchmarks.add(it) }

    operator fun String.invoke(testObj: T) {
        _benchmarks.firstOrNull { it.name == this }?.bench?.invoke(testObj)
            ?: (_before + _after).firstOrNull { it.first == this }
                ?.second?.invoke(testObj)
            ?: throw FunctionNotFoundException(this)
    }

    @BenchmarkDsl
    fun String.beforeBench(before: (T) -> Unit) {
        _before.add(this to before)
    }

    @BenchmarkDsl
    fun String.afterBench(after: (T) -> Unit) {
        _after.add(this to after)
    }

    private data class BenchResult(
        var name: String,
        var iter: Int,
        var avgMilliTime: Double,
        var fullTimeMilli: Long
    )

    private suspend fun bench(testObj: T, kBench: KBench<T>): BenchResult {
        log("\tRunning KBench: '${kBench.name}' (${kBench.iterations} iterations)")
        val (avg: Double, time: TimeSpan) = measureTimeWithResult {
            List(kBench.iterations) {
                kBench.pre?.invoke(testObj)
                (withTimeoutOrNull(kBench.timeOutMilli) {
                    measureTime { kBench.bench(testObj) }.millisecondsLong
                } ?: kBench.timeOutMilli)
                    .also { kBench.post?.invoke(testObj) }
            }.average()
        }
        verbose("\t...Completed KBench: '${kBench.name}'")
        return BenchResult(kBench.name, kBench.iterations, avg, time.millisecondsLong)
    }

    open fun run() = runTest {
        log("-" * 80)
        log(" " * 35 + " KBench")
        log("-" * 80)
        log("Benchmarking...")
        val res: List<BenchResult> = _benchmarks.map { kbench ->
            val t = generate()
            _before.forEach { it.second(t) }
            bench(t, kbench)
                .also { _after.forEach { it.second(t) } }
        }
        log("...Benchmark complete. Here are the results:")
        res.forEach {
            log(it)
        }
        log("-" * 80)
    }

    companion object {
        val AVERAGE_BY_SIZE = 1_000
    }
}
