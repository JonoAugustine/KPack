/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package com.ampro.kpack.kache

import com.ampro.kpack.collections.LinkedList
import com.ampro.kpack.collections.Node

/**
 * A Caching Interface which presents a framework for abstracting away from a [Map],
 * allowing for more detailed internal control over caching behavior.
 *
 * @param K Key type
 * @param V Value type
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
interface Kache<K, V> : Iterable<Map.Entry<K, V>> {

    /** An immutable clone of the cache's current state. */
    val image: Map<K, V>
    /** Returns the number of [key][K]-[value][V] pairs in the map. */
    val size: Int
    /** An immutable list of the cache's keys. */
    val entries: Set<Map.Entry<K, V>>
    /** An immutable list of the cache's keys. */
    val keys: Set<K>
    /** An immutable list of the cache's values. */
    val values: List<V>

    /** Get the value associated with the [key] from cache. */
    fun get(key: K): V?

    /**
     * Add a [key]-[value] pair to the cache.
     * Returns the previous value associated with the [key].
     */
    fun put(key: K, value: V): V?

    /**
     * Add a [key]-[value] pair to the cache.
     * Returns the previous value associated with the [key].
     */
    operator fun set(key: K, value: V) {
        put(key, value)
    }

    fun putAll(from: Map<out K, V>) = from.forEach { (k, v) -> this[k] = v }

    /** Remove a [key]-[value][V] entry from cache. Returns the removed [value][V]. */
    fun remove(key: K): V?

    /** Remove a [key]-[value][V] entry from cache. */
    operator fun minusAssign(key: K) {
        this.remove(key)
    }

    operator fun plusAssign(entry: Pair<K, V>) {
        this[entry.first] = entry.second
    }

    /** `true` if the [key] exists in the cache. */
    operator fun contains(key: K): Boolean

    fun containsValue(value: V): Boolean
    fun containsKey(key: K): Boolean = contains(key)
    fun isEmpty(): Boolean
    fun clear()
    override fun iterator(): Iterator<Map.Entry<K, V>> = entries.iterator()
}

/**
 * A [Kache] implementation which attempts to [load] values
 * back into cache when not found.
 *
 * @param K Key type
 * @param V Value type
 * @property load The load function to generate entries when not found in cache.
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
interface ExtendedKache<K, V> : Kache<K, V> {
    val load: (K) -> V?
}

/**
 * A [Kache] implementation which prioritizes the usage frequency of entries.
 *
 * @param K Key type
 * @param V Value type
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
abstract class UsagePriorityKache<K : Any, V> : Kache<K, V> {

    protected val map: MutableMap<K, V> = mutableMapOf()
    override val image: Map<K, V> get() = map.toMap()
    override var size: Int = 0

    override val entries: Set<Map.Entry<K, V>> get() = map.toMap().entries
    override val keys: Set<K> get() = map.keys
    override val values: List<V> get() = map.values.toList()

    /**
     * A [LinkedList] implementation that allows for instant access to a node through a
     * [HashMap].
     */
    protected open inner class UsageList : LinkedList<K>() {

        protected open val hashmap = HashMap<K, Node<K>>()

        init {
            head.apply { data?.also { hashmap[it] = this } }
            tail.apply { data?.also { hashmap[it] = this } }
        }

        override fun prepend(element: K) {
            hashmap[element]?.also {
                // If key exists, disconnect it
                // Connect prev to n.next. n.prev is never a head/tail
                it.prev!!.next = it.next
                // Connect next to prev
                it.next!!.prev = it.prev
            }
            super.addAfter(head, element).also { hashmap[element] = it }
        }

        override fun append(element: K) {
            hashmap[element]?.also {
                // If key exists, disconnect it
                // Connect prev to n.next. n.prev is never a head/tail
                it.prev!!.next = it.next
                // Connect next to prev
                it.next!!.prev = it.prev
            }
            super.addAfter(tail.prev!!, element).also { hashmap[element] = it }
        }

        override fun removeLast(): K? = super.removeLast()?.also { hashmap -= it }

        override fun removeFirst(): K? = super.removeFirst()?.also { hashmap -= it }

        override operator fun minusAssign(key: K) {
            take(key)
        }

        open fun take(element: K): K? = hashmap.remove(element)?.also { node ->
            node.prev!!.next = node.next
            node.next!!.prev = node.prev
            size--
        }?.data

        override fun clear() {
            super.clear()
            hashmap.clear()
        }
    }

    /** An internal list used to track the usage of entries. */
    protected open val usageRanks = UsageList()

    /**
     * The entry to remove when the list has reached capacity
     * and needs to insert a new value.
     */
    protected abstract val evictTarget: K?

    /** Clear the [Kache]'s internal [map] and [usageRanks]. */
    override fun clear() {
        size = 0
        map.clear()
        usageRanks.clear()
    }

    override fun contains(key: K): Boolean = map.containsKey(key)
    override fun containsValue(value: V): Boolean = map.containsValue(value)

    override fun isEmpty(): Boolean = size == 0

    companion object {
        const val DEFAULT_MIN = 100
        const val DEFAULT_MAX = 10_000
    }
}
