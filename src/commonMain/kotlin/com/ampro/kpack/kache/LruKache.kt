/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package com.ampro.kpack.kache

/**
 * An Implementation of [UsagePriorityKache] which removes [trashSize]-number of the
 * least recently used entry when space is needed. Stores by [key][K]/[value][V] pairs,
 * and takes the [minimum size][minSize] and [maximum size][maxSize] of the cache as
 * constructor parameters.
 *
 * See [LRU](https://en.wikipedia.org/wiki/Cache_replacement_policies#LRU)
 *
 * @param K Key type
 * @param V Value type
 *
 * @property minSize The minimum size the [LruKache] will self reduce to during downsizing.
 * *This takes priority over [trashSize]*.
 * @property maxSize the maximum number of entries allowed before new entries will
 * cause downsizing.
 * @property trashSize The number of elements to remove during a downsizing.
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
open class LruKache<K : Any, V>(
    open val maxSize: Int = DEFAULT_MAX,
    open val minSize: Int = DEFAULT_MIN,
    open val trashSize: Int = DEFAULT_TRASH_SIZE
) : UsagePriorityKache<K, V>() {

    override val evictTarget get() = usageRanks.removeLast()

    init {
        if (trashSize < 1) {
            throw IllegalArgumentException("LRU TrashSize must be greater than 0.")
        }
    }

    /**
     * Set a [Key][K]-[Value][V] pair in cache. If the cache is at [maxSize],
     * remove [trashSize]-number [entries][evictTarget] then add the new entry.
     * @return the [value][V] previously at [key]
     */
    override fun put(key: K, value: V): V? {
        // Downsize on max-size
        if (key !in this && size == maxSize) {
            var i = 1
            while (size > minSize && i++ < trashSize)
                evictTarget?.let(this::remove) ?: break // break if empty
        }
        return map.put(key.apply(usageRanks::prepend), value).also { size++ }
    }

    /** Returns the [value][V] associated with the [key] and sets it to most recently used. */
    override fun get(key: K): V? = map[key]?.also { usageRanks.prepend(key) }

    override fun remove(key: K): V? = map.remove(key)?.also {
        usageRanks -= key
        size--
    }

    companion object {
        const val DEFAULT_TRASH_SIZE = 1
    }
}

/**
 * A Kache implementation combining [ExtendedKache] and [LruKache].
 *
 * @param K Key type
 * @param V Value type
 *
 * @param maxSize the maximum number of entries allowed before new entries will
 * cause downsizing.
 * @param minSize The minimum size the [LruKache] will self reduce to during downsizing.
 * *This takes priority over [trashSize]*.
 * @param trashSize The number of elements to remove during a downsizing.
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
class ExtendedLruKache<K : Any, V>(
    maxSize: Int = DEFAULT_MAX,
    minSize: Int = DEFAULT_MIN,
    trashSize: Int = DEFAULT_TRASH_SIZE,
    override val load: (K) -> V?
) : LruKache<K, V>(maxSize, minSize, trashSize), ExtendedKache<K, V> {
    override fun get(key: K): V? = super.get(key) ?: load(key)?.also { put(key, it) }
}
