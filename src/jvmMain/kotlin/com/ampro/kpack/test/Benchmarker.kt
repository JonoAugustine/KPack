/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package com.ampro.kpack.test

import kotlinx.coroutines.runBlocking

actual fun runTest(f: suspend () -> Unit) = runBlocking { f() }
