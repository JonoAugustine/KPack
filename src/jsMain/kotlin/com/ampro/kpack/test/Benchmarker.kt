/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package com.ampro.kpack.test

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.promise

actual fun runTest(f: suspend () -> Unit): dynamic = GlobalScope.promise { f() }
