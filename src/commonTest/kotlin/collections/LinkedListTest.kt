/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package collections

import com.ampro.kpack.collections.LinkedList
import com.ampro.kpack.collections.removeRandom
import testIntList
import kotlin.random.Random
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue

/**
 * Unit testing for [LinkedList] using [Int] as type.
 *
 * @author Jonathan Augustine
 * @since 1.0.0
 */
class LinkedListTest {

    lateinit var list: LinkedList<Int>

    private fun fill(size: Int = 1_000) = list.appendAll(testIntList(size))

    @BeforeTest
    fun resetMap() {
        list = LinkedList()
    }

    // Construction tests

    @Test
    fun empty_on_blank_construct() {
        assertTrue { list.isEmpty() }
        assertEquals(0, list.size)

        assertTrue { LinkedList(size = 0) {}.isEmpty() }
        assertEquals(0, LinkedList(size = 0) {}.size)

        assertEquals(0, LinkedList(elements = *emptyArray()).size)
        assertTrue { LinkedList(elements = *emptyArray()).isEmpty() }
    }

    @Test
    fun not_empty_on_param_construct() {
        // Index builder
        assertEquals(1_000, LinkedList(1_000) { it }.size)
        assertFalse { LinkedList(1_000) { it }.isEmpty() }

        // Source iterable
        assertEquals(1_000, LinkedList(testIntList(1_000)).size)
        assertFalse { LinkedList(testIntList(1_000)).isEmpty() }

        // Vararg
        assertEquals(1_000, LinkedList(*Array(1_000) { it }).size)
        assertFalse { LinkedList(*Array(1_000) { it }).isEmpty() }
    }

    // Mutate tests

    @Test
    fun prepend_adds_to_front() = testIntList().forEach { i ->
        list.prepend(i)
        assertEquals(i, list.front)
    }

    @Test
    fun append_adds_to_back() = testIntList().forEach { i ->
        list.append(i)
        assertEquals(i, list.back)
    }

    @Test
    fun append_prepend_alternate() = testIntList().forEach { i ->
        when (Random(1998).nextInt() % 2) {
            0 -> {
                list.append(i)
                assertEquals(i, list.back)
            }
            else -> {
                list.prepend(i)
                assertEquals(i, list.front)
            }
        }
    }

    @Test
    fun append_all() {
        list.appendAll(testIntList())
        list.forEach { i -> assertEquals(i, list[i]) }
    }

    @Test
    fun prepend_all() {
        list.appendAll(testIntList())
        list.toList().asReversed().forEach { i -> assertEquals(i, list[i]) }
    }

    @Test
    fun get_index() = testIntList().forEach { i ->
        list.append(i * 2)
        assertEquals(i * 2, list[i])
    }

    @Test
    fun remove_first() {
        list.appendAll(testIntList())
        for (i in 0 until 1_000) assertEquals(i, list.removeFirst())
    }

    @Test
    fun remove_last() {
        list.appendAll(testIntList())
        for (i in 999 downTo 0) assertEquals(i, list.removeLast())
    }

    @Test
    fun remove_index() {
        list.appendAll(testIntList())
        assertEquals(69, list.removeAt(69))
        assertEquals(70, list.removeAt(69))
    }

    @Test
    fun remove_element() {
        list.appendAll(testIntList())
        val tl = testIntList().toMutableList()
        while (tl.isNotEmpty()) {
            tl.removeRandom().also { n -> assertEquals(n, list.remove(n)) }
        }
    }

    @Test
    fun remove_all() {
        list.appendAll(testIntList())
        list.removeAll(testIntList().subList(50, 700))
        assertFalse { list.containsAll(testIntList().subList(50, 700)) }
    }

    @Test
    fun retain_all() {
        list.appendAll(testIntList())
        list.retainAll(testIntList().subList(50, 700))
        assertTrue { list.containsAll(testIntList().subList(50, 700)) }
    }

    @Test
    fun clear() {
        list.appendAll(testIntList())
        list.clear()
        assertEquals(0, list.size)
        assertTrue { list.isEmpty() }
        assertNull(list.front)
        assertNull(list.back)
    }

    @Test
    fun contains_single() {
        list.appendAll(testIntList())
        testIntList().forEach { n -> assertTrue { list.contains(n) } }
    }

    @Test
    fun contains_all() {
        list.appendAll(testIntList())
        assertTrue { list.containsAll(testIntList()) }
    }

    // Variable Tests

    @Test
    fun first_is_first() {
        // First stays front
        for (i in 0 until 1_000) {
            list.append(i)
            assertEquals(0, list.front)
        }
        resetMap()
        // First changes on prepend
        for (i in 0 until 1_000) {
            list.prepend(i)
            assertEquals(i, list.front)
        }
        resetMap()
        // Alternate pre/append
        for (i in 0 until 1_000) {
            list.prepend(i * -1)
            assertEquals(i * -1, list.front)
            list.append(i)
        }
    }

    @Test
    fun last_is_last() {
        // back moves on append
        for (i in 0 until 1_000) {
            list.append(i)
            assertEquals(i, list.back)
        }
        resetMap()
        // First changes on prepend
        for (i in 0 until 1_000) {
            list.prepend(i)
            assertEquals(0, list.back)
        }
        resetMap()
        // Alternate pre/append
        for (i in 0 until 1_000) {
            list.append(i * -1)
            assertEquals(i * -1, list.back)
            list.prepend(i)
        }
    }

    @Test
    fun size_up_on_append() {
        for (i in 1..1_000) {
            list.append(i)
            assertEquals(i, list.size)
        }
    }

    @Test
    fun size_up_on_prepend() {
        for (i in 1..1_000) {
            list.prepend(i)
            assertEquals(i, list.size)
        }
    }

    @Test
    fun size_up_on_alternating() {
        for (i in 1..1_000) {
            when (Random(1998).nextInt() % 2) {
                0 -> list.append(i)
                else -> list.prepend(i)
            }
            assertEquals(i, list.size)
        }
    }

    @Test
    fun size_down_on_remove_front() {
        list.appendAll(testIntList())
        for (i in testIntList().asReversed()) {
            list.removeFirst()
            assertEquals(i, list.size)
        }
    }

    @Test
    fun size_down_on_remove_back() {
        list.appendAll(testIntList())
        for (i in testIntList().asReversed()) {
            list.removeLast()
            assertEquals(i, list.size)
        }
    }

    @Test
    fun size_down_on_remove_element() {
        list.appendAll(testIntList())
        for (i in testIntList().asReversed()) {
            list.remove(list.random())
            assertEquals(i, list.size)
        }
    }

    @Test
    fun size_down_on_remove_index() {
        list.appendAll(testIntList())
        for (i in testIntList().asReversed()) {
            list.removeAt(Random(0).nextInt(list.size))
            assertEquals(i, list.size)
        }
    }

    // Iterators

    @Test
    fun iterator() {
        list.appendAll(testIntList())
        list.forEachIndexed { index, i ->  assertEquals(index, i) }
    }

    @Test
    fun inverse_iterator() {
        list.appendAll(testIntList())
        var i = 999
        for (n in list.inverseIterator()) assertEquals(i--, n)
    }

}
