/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package collections

import com.ampro.kpack.collections.Graph
import com.ampro.kpack.collections.contains
import com.ampro.kpack.collections.find
import com.ampro.kpack.collections.set
import testIntList
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

// TODO More tests

class GraphTest : Graph<Int, Int>() {

    @BeforeTest
    fun clean() {
        edgeCount = 0
        adjacency.clear()
    }

    @Test
    fun empty_on_init() {
        assertTrue { isEmpty }
        assertTrue { hasNoConnections }
        assertEquals(0, vertexCount)
        assertEquals(0, edgeCount)
    }

    @Test
    fun put_adds_to_map() {
        testIntList().forEach {
            assertNull(putVertex(it))
            assertTrue { it in this }
        }
    }

    @Test
    fun put_ejects_old() {
        put_adds_to_map()
        testIntList().forEach {
            val old = putVertex(it)
            assertNotNull(old)
            assertEquals(old.size, find(it).size)
        }
    }

    @Test
    fun getOrPut_gets_or_adds() {
        val edgeSet_pre = getOrPutVertex(0)
        assertEquals(0, edgeSet_pre.size)
        remove(0)
        connect_links_vertexes()
        val edgeSet = getOrPutVertex(0)
        assertEquals(2, edgeSet.size)
    }

    @Test
    fun connect_links_vertexes() {
        put_adds_to_map()
        (0 until 1_000 step 2).forEach { i ->
            this[i, i + 1] = i * -1
            assertEquals(i * -1, getConnection(i, i + 1))
            assertEquals(i * -1, i getEdgeTo i + 1)
            this[i + 1, i] = i * -2
            assertEquals(i * -2, getConnection(i + 1, i))
            assertEquals(i * -2, i + 1 getEdgeTo i)
        }
    }

    @Test
    fun edge_size_increases_on_connect() {
        put_adds_to_map()
        (0 until 1_000 step 2).forEach { i ->
            assertEquals(i, edgeCount)
            this[i, i + 1] = i * -1
            assertEquals(i + 1, edgeCount)
            this[i + 1, i] = i * -2
            assertEquals(i + 2, edgeCount)
        }
    }
}
