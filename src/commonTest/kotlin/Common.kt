/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

/** get an [Int] list 0 until size] (default 1_000 */
fun testIntList(size: Int = 1_000): List<Int> = List(size) { it }
