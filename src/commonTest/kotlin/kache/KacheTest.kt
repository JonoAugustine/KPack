/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package kache

import com.ampro.kpack.kache.ExtendedKache
import com.ampro.kpack.kache.Kache
import com.ampro.kpack.kache.LruKache
import com.ampro.kpack.kache.UsagePriorityKache
import kotlin.test.Test

/**
 * TODO
 *
 * @param K
 * @param V
 */
interface KacheTest<K : Any, V, C : Kache<K, V>> {
    var kache: C

    @Test
    fun image()

/*
size
entries
keys
values
get
put
set
putAll
remove
minusAssign
plusAssign
contains
containsValue
containsKey
isEmpty
clear
iterator
 */
}

interface UsagePriorityKacheTest<K : Any, V, C : UsagePriorityKache<K, V>> :
    KacheTest<K, V, C>

interface ExtendedKacheTest<K : Any, V, C : ExtendedKache<K, V>> : KacheTest<K, V, C>

open class LruKacheTest : UsagePriorityKacheTest<Int, Int, LruKache<Int, Int>> {
    override lateinit var kache: LruKache<Int, Int>

    override fun image() {
        TODO("image not implemented")
    }
}
