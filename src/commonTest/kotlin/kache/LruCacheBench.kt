/*******************************************************************************
 * Copyright (c) 2019, High Friction by Jonathan Glen Augustine
 ******************************************************************************/

package kache

import com.ampro.kpack.kache.LruKache
import com.ampro.kpack.test.Benchmarker
import testIntList
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertTrue

class LruCacheBench : Benchmarker<LruKache<Int, Int>>({
    verbose_log = true
    generate = { LruKache(maxSize = 1_000, minSize = 100, trashSize = 10) }

    "setup".beforeBench { assertTrue { it.isEmpty() } }

    "fill" { cache -> testIntList(cache.maxSize).forEach { cache[it] = it } }

    "overfill"(pre = { "fill"(it) }) { it.put(it.maxSize + 10, -1) }

    "get"(pre = { "fill"(it) }) { it.get(Random(1869).nextInt(it.maxSize)) }
}) {
    @Test override fun run() = super.run()
}
